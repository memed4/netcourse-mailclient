Report
============================

##Socket API

- getaddrinfo(): resolve domain names
- socket(): create socket
- connect(): establish TCP connection
- close(): close connection
- read()/write(): read / write from socket

##POP3

Messages to exchange:

S: +OK  
C: USER username  
S: +OK  
C: PASS password  
S: +OK  
C: LIST  
S: +OK  
S: no0 len0  
S: no1 len1   
S: .  
C: RETR no0  
S: data...  
S: .  
[optional] C: DELE no0  
[optional] S: +OK  
...  
C: QUIT   
S: +OK  


##SMTP

Messages to exchange:

S: 250
C: EHLO ip/dname  
S: 250-...   
S: 250-AUTH ... 
S: 250 ...  
[alt1] C: AUTH LOGIN  
[alt1] S: 334 VXNlcm5hbWU6  
[alt1] C: "username@domain".encode("base64")  
[alt1] S: 334 UGFzc3dvcmQ6  
[alt1] S: 235  
[alt2] C: AUTH PLAIN  
[alt2] S: 334  
[alt2] C: "username\0username\0password".encode("base64")  
[alt2] S: 235  
C: MAIL FROM: <username@domain\>  
S: 250  
C: RCPT TO: <receipt mail address\>  
S: 250  
...  
C: DATA  
S: 354  
C: Date: Mon, 27 Apr 2015 22:16:00 +0800  
C: From: "name" <username@domain\>  
C: To: "name1" <addr1>,  
C:  "name2" <addr2>  
C: cc: "name" <addr>  
C: Subject: ...  
C:  
C: mail body  
C: .  
S: 250   
C: QUIT    
S: 221  
