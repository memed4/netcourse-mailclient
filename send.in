to: "zzy" <zzy_client_test1@163.com>, 
 "ZhaoZhuoyue" <zzy7896321@163.com>
cc: "zzysjtu" <zzy7896321@sjtu.edu.cn>
SUBJECT: README of my mail client

A Mail Client
============================
Assignment 1 of network course at SJTU. A simple POP3 and SMTP
client program. 

NOTE: Messages are sent in clear text.

## Build
A C/C++ compiler with c++11/c11 support is needed.
The Makefile defaults to use gcc/g++. Change the CC, CXX,
M_CFLAGS, M_CXXFLAGS to use other compilers.

To compile, type

	make

To clean, type
	
	make clean

To also clean the dependencies, type

	make cleanall

## Run POP3 Client

	bin/pop3_main [-h|--help] [-d|--delete] [outfile] [server] [port] [user] [password]

If -d or --delete is given, received mails are deleted from the server.

If outfile is - or not given, received mails are written to stdout.

If any of the following arguments are not given, they will be asked 
from the console.

An example:

	bin/pop3_main - pop3.163.com 110 zzy_client_test1

## Run SMTP Client

	bin/smtp_main [infile] [server] [port] [mail_addr] [password]
	bin/smtp_main -h
	bin/smtp_main --help

If infile is - or not given, the mail to send is read from stdin.
End the mail with a EOF (Ctrl-D).

If any of the following arguments are not given, they will be asked
from the console.

An example is 

	bin/smtp_main - smtp.163.com 25 zzy_client_test1@163.com

The format of the input should conform to RFC822. The mail should 
consists of structured headers, an empty line and the unstructured
mail text.

From, to, cc fields are specially treated in that
they are parsed and checked. The format of the recipient 
should be

	name <local-part@domain>

where name is optional.

From, to, cc, date and subject fields are case-insensitive in the
input and they will be properly transformed to the correct case
prior to the transmission. 
Other fields are copied verbatim to the data buffer.

Regardless whether the input contains To/cc, 
additional recipients will be asked after the input is read from
the console. End the input with an empty line to indicate there's
no additional recipients. If the input does not contain a subject,
the subject will also be asked from the console.


-----------------------------
Zhao Zhuoyue
ACM Class, Shanghai Jiao Tong University

