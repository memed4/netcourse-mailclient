#ifndef AMC_CONFIG_H
#define AMC_CONFIG_H

#include <sys/types.h>

#if __cplusplus < 201103L
#	error "need c++11 support"
#endif

#ifdef __GNUC__
#	define amc_attribute __attribute__
#else
#	define amc_attribute(x)
#endif

/* default read buffer size */
#define DEFAULT_BUFFER_SIZE 1024

/* default buffer size of util format_string function */
#define DEFAULT_FSTRING_BUF_SIZE 1024

#include <string>
#include <sstream>
#ifdef __CYGWIN__
template<typename T>
std::string to_string(T value) {
	std::ostringstream oss;
	oss << value;
	return oss.str();
}
#else
using std::to_string;
#endif

#endif

