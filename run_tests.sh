#!/bin/bash

test_cnt=0
pass_cnt=0

function run_test() {
	progname="$1"
	shift
	test_cnt=$(expr ${test_cnt} "+" 1)
	echo -n "test #${test_cnt}: ${progname}"
	echo "test #${test_cnt}: ${progname}" >> ${logfile}
	"${bindir}/${progname}" "$@" 2&>>${logfile}
	if [[ $? -ne 0 ]]; then
		echo "       FAIL"	
		echo "FAIL" >> ${logfile}
	else
		echo "       PASS"
		echo "PASS" >> ${logfile}
		pass_cnt=$(expr ${pass_cnt} "+" 1)
	fi
	echo "" >> ${logfile}
}

if [[ $# < 3 ]]; then
	echo "usage: $0 <bindir> <tmpdir> <logfile>"
	exit 1
fi

bindir="$1"
tmpdir="$2"
logfile="$3"

rm -f "${logfile}"

rm -rf "${tmpdir}"
mkdir "${tmpdir}"

run_test test_debug_level_logger ${tmpdir}/test_debug_level_logger.out
run_test test_error_level_logger ${tmpdir}/test_error_level_logger.out
run_test test_readline
run_test test_base64

echo ""
echo "Passed/Tested: ${pass_cnt}/${test_cnt}"
echo "Passed/Tested: ${pass_cnt}/${test_cnt}" >> ${logfile}


if [[ ${test_cnt} -ne ${pass_cnt} ]]; then
	exit 1
else
	exit 0
fi

