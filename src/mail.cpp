#include "config.h"

#include "errs.h"
#include "mail.h"

#include <algorithm>

auto mail_t::get_sorted_headers() const 
	-> std::vector<decltype(std::cref(*headers.begin()))>
{
	std::vector<decltype(std::cref(*headers.begin()))> 
		sorted_headers(headers.begin(), headers.end());

	typedef decltype(sorted_headers.front().get()) P;
	std::sort(sorted_headers.begin(), sorted_headers.end(),
		[](const P& x, const P& y) -> bool {
			return x.second.seq_no < y.second.seq_no;
		});

	return std::move(sorted_headers);
}

void mail_t::get_mail_body(std::string &body, const char *eoln) 
const {
	auto sorted_headers(get_sorted_headers());

	typedef decltype(sorted_headers.front().get()) P;
	body.clear();
	body.reserve(text.length() + 4096);
	for (const P& h : sorted_headers) {
		body.append(h.first).append(": ")
			.append(h.second.value).append(eoln);
	}

	body.append(eoln).append(text).append(".");
}


bool mail_t::get_comma_separated_values(const std::string& field, std::vector<std::string> &values) const {
	const mail_header_t* h;
	get_field(field, &h);
	if (!h || h->value.empty()) return false;
	
	auto p = 0;
	auto i = h->value.find(",\r\n ");
	for ( ; i != std::string::npos; 
		p = i, i = h->value.find(",\r\n ", i)) {
		
		values.push_back(h->value.substr(p, i - p));
		i += 4;
	}
	values.push_back(h->value.substr(p, std::string::npos));

	return true;
}

void dump_mail(std::ostream& os, const mail_t& mail) {
	std::string m;
	mail.get_mail_body(m, "\n");
	os << m << std::endl;
}
