#include "connection.h"
#include "errs.h"

#include <algorithm>

connection_t::connection_t(EOLN_style _eoln_style,
		size_t _buf_size):
	eoln_style(_eoln_style),
	buf_size(_buf_size), buf_head(0), 
	buf_n(0), buffer(nullptr) {
	
	if (eoln_style < 0 || eoln_style >= NEOLN_STYLE) {
		log_warn(COMP_CONNECTION, "invalid eoln style, defaults to CRLF\n");
		eoln_style = EOLN_CRLF;
	}

	try {
		buffer = new char[buf_size];
	}
	catch (std::bad_alloc) {
		throw_fatal(COMP_CONNECTION, "out of memory");
	}
}

connection_t::~connection_t() {
	delete [] buffer;
}

ssize_t connection_t::read(char *out_buffer, size_t buf_size) noexcept {
	ssize_t len_in_buf = 0;
	if (buf_head < buf_n) {
		len_in_buf = std::min((ssize_t) buf_size, buf_n - buf_head);
	}

	ssize_t want_len_from_io = buf_size - len_in_buf;
	ssize_t len_from_io = 0;
	if (want_len_from_io > 0) {
		len_from_io = read_no_buf(
				out_buffer + len_in_buf, want_len_from_io);
		if (len_from_io < 0) {
			/* error */
			return len_from_io;
		}
	}

	if (len_in_buf > 0) {
		std::copy(buffer + buf_head, buffer + buf_head + len_in_buf,
				out_buffer);
		buf_head += len_in_buf;
	}

	return len_in_buf + len_from_io;
}

void connection_t::readline(std::string &line) {
	log_verbose(COMP_CONNECTION, "readline");
#ifndef NDEBUG
	auto last_char = line.length();
#endif
	ssize_t eoln_pos, n;
	
	while (true) {
		if (buf_head < buf_n) {
			eoln_pos = find_eoln(buffer + buf_head, buf_n - buf_head) + buf_head;

			line.append(buffer + buf_head, buffer + eoln_pos);
			if (eoln_pos + eoln_skip_cnt[eoln_style] <= buf_n) {
				/* found */
				buf_head = eoln_pos + eoln_skip_cnt[eoln_style];
				break;
			}
			else {
				/* reset head and n, and read more */
				if (eoln_pos != 0 && eoln_pos < buf_n) {
					std::copy(buffer + eoln_pos, buffer + buf_n, buffer);
				}

				buf_n = buf_n - eoln_pos;
				buf_head = 0;
			}
		}
		else {
			buf_head = buf_n = 0;
		}

		n = read_no_buf(buffer + buf_n, buf_size - buf_n);
		if (n < 0) {
			throw_fatal(COMP_CONNECTION, "read error");
		}
		else if (n == 0) {
			throw_error(COMP_CONNECTION, "incomplete line");
		}
		buf_n += n;
	}
	
	log_verbose_f(COMP_CONNECTION, "%s", line.substr(last_char, -1).c_str());
}

ssize_t connection_t::find_eoln(char *buffer, ssize_t buf_n) noexcept {
	switch (eoln_style) {
	case EOLN_CR:
		return std::find(buffer, buffer + buf_n, '\r') - buffer; 
		break;
	case EOLN_LF:
		return std::find(buffer, buffer + buf_n, '\n') - buffer;
		break;
	case EOLN_CRLF:
		{
			ssize_t i = 0;
			while (i + 1 < buf_n) {
				if (buffer[i] == '\r' && buffer[i + 1] == '\n')	{
					return i;
				}
				++i;
			}
			return (buffer[i] == '\r') ? i : (i + 1);
		}
		break;
	default:
		/* to suppress -Wswitch warning */;
	}

	return -1;
}

ssize_t connection_t::write(const char *buffer, size_t len) noexcept {
	size_t cur = 0;
	int n;
	while (cur < len) {
		n = write_no_buf(buffer + cur, len - cur);
		if (n <= 0) {
			return cur;
		}
		
		cur += n;
	}

	return cur;
}

void connection_t::writeline(std::string msg) {
	log_verbose_f(COMP_CONNECTION, "writeline %s", msg.c_str());
	msg += get_eoln();
	ssize_t s = write(msg.c_str(), msg.length());
	if (s < (ssize_t) msg.length())
		throw_fatal(COMP_CONNECTION, "write error");
}

void connection_t::resize_buffer(size_t buf_size) noexcept {
	char *new_buffer;
	if (buf_size != this->buf_size) {
		try {
			new_buffer = new char[buf_size];
			delete [] buffer;
			this->buf_size = buf_size;
			buffer = new_buffer;
		} catch (std::bad_alloc) {
			log_warn_f(COMP_CONNECTION, "out of memory %llu", 
					(unsigned long long) buf_size);
		}
	}
	buf_head = buf_n = 0;
}

const char *connection_t::eoln_cstr[connection_t::NEOLN_STYLE] = {
	"\r\n",
	"\r",
	"\n",
};

const ssize_t connection_t::eoln_skip_cnt[connection_t::NEOLN_STYLE] = {
	2,
	1,
	1,
};

