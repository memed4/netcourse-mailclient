#include "config.h"
#include "errs.h"
#include "sock_wrapper.h"

#include <cerrno>
#include <cstring>

void getaddrinfo_w(const char *hostname, const char *service,
		const struct addrinfo *hints, struct addrinfo **result) {
	
	log_debug_f(COMP_SYS, "getaddrinfo(%s, %s)", hostname, service);

	int ret;
	while ((ret = getaddrinfo(hostname, service, hints, result))) {
		if (ret == EAI_AGAIN) {
			log_warn_f(COMP_SYS, "retry getaddrinfo(%s, %s)", 
					hostname, service);
		}

		if (ret == EAI_SYSTEM) {
			ret = errno;
			throw_error_eno(COMP_SYS, ret, strerror(ret));
		}

		throw_error_eno(COMP_SYS, ret, gai_strerror(ret));
	}
}


