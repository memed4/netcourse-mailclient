#include "config.h"
#include "client_socket.h"

#include "sock_wrapper.h"
#include "errs.h"
#include <cstring>

client_socket_t::~client_socket_t() {
	close();
}

void client_socket_t::init_conn(std::string host, std::string port) {
	
	struct addrinfo hints, *res_head, *res_cur;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	
	getaddrinfo_w(host.c_str(), port.c_str(), &hints, &res_head);

	res_cur = res_head;
	while (res_cur) {
		sockfd = socket(res_cur->ai_family, res_cur->ai_socktype, 
				res_cur->ai_protocol);
		if (sockfd < 0) {
			continue;
		}
		if (connect(sockfd, res_cur->ai_addr, res_cur->ai_addrlen) == 0)
			break;

		::close(sockfd); sockfd = -1;
		res_cur = res_cur->ai_next;
	}

	if (sockfd == -1) {
		throw_error_f(COMP_CONNECTION, "cannot connect to (%s, %s)",
				host.c_str(), port.c_str());
	}
}

void client_socket_t::close() {
	::close(sockfd);
	sockfd = -1;
}

ssize_t client_socket_t::read_no_buf(char *buffer, size_t buf_size) noexcept {
	if (sockfd == -1) return -1;
	return ::read(sockfd, buffer, buf_size);
}

ssize_t client_socket_t::write_no_buf(const char *buffer, size_t len) noexcept {
	if (sockfd == -1) return -1;
	return ::write(sockfd, buffer, len);
}

bool client_socket_t::get_local_name(std::string *name, short *port) {
	if (!name && !port) return true;

	sockaddr_storage ss;
	socklen_t len = sizeof(ss);
	int ret = getsockname(sockfd, (sockaddr*) &ss, &len);
	if (ret) {
		return false;
	}
	
	switch (ss.ss_family) {
	case AF_INET:
	{
		sockaddr_in *in = (sockaddr_in*) &ss;
		if (name) {
			char buffer[INET_ADDRSTRLEN];
			if (inet_ntop(AF_INET, &in->sin_addr,
					buffer, INET_ADDRSTRLEN)) {
				*name = buffer;
			}
			else return false;	
		}

		if (port) *port = ntohs(in->sin_port);
	}
	case AF_INET6:
	{
		sockaddr_in6 *in6 = (sockaddr_in6*) &ss;
		if (name) {
			char buffer[INET6_ADDRSTRLEN];
			if (inet_ntop(AF_INET6, &in6->sin6_addr,
						buffer, INET6_ADDRSTRLEN)) {
				*name = buffer;
			} else return false;
		}

		if (port) *port = ntohs(in6->sin6_port);
	}
	default:
		return false;
	}
	return true;
}

