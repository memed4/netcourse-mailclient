#include "config.h"
#include "errs.h"
#include "pop3_client.h"
#include "utils.h"

#include <iostream>
#include <cstring>
using std::cin;
using std::cout;
using std::endl;

void pop3_main_usage(const char *progname) {
	cout << "usage: " << progname << " [-h|--help] [-d|--delete] [outfile]"
		" [server] [port] [user] [password]" << endl;
}

int main(int argc, char *argv[]) {
	
	bool del = false;
	int iarg = 1;
	while (iarg < argc && argv[iarg][0] == '-') {
		if (argv[iarg][1] == '\0') break;
		if (!strcmp("-h", argv[iarg]) || !strcmp("--help", argv[iarg])) {
			pop3_main_usage(argv[0]);
			return 0;
		}

		else if (!strcmp("-d", argv[iarg]) || !strcmp("--delete", argv[iarg])) {
			del = true;	
		}
		else {
			cout << "unknown option: " << argv[iarg] << endl;
			return 1;
		}
		++iarg;
	}

	init_default_event_loggers(LLEVEL_INFO, nullptr);
	
	std::string outfile = (iarg < argc) ? argv[iarg++] : "-";
	std::string server = (iarg < argc) ? argv[iarg++] : get_input("server");
	std::string port = (iarg < argc) ? argv[iarg++] : get_input("port");
	std::string user = (iarg < argc) ? argv[iarg++] : get_input("user");
	std::string pass = (iarg < argc) ? argv[iarg++] : get_pass("password");
	
	pop3_client_t pop3(server, port, user, pass);
	try {
		if (outfile != "-")
			pop3.recv_all(outfile, del);
		else
			pop3.recv_all(del);
	}
	catch (...) {
		std::cout << "error" << std::endl;
	}

	return 0;
}
