#include "logging.h"

#include <list>
#include <cstdio>
#include <sstream>

void init_default_event_loggers(
		logging_level_t level,
		const char* log_filename) {
	add_file_logger(level, stdout);
	if (log_filename) {
		add_file_logger(level, log_filename, "w");
	}
}

static std::string component_string[N_COMPONENT] = {
	"unknown",
	"connection_t",
	"other",
	"system",
	"pop3",
	"smtp",
	"main",
};

const std::string& strcomponent(component_t comp_id) {
	return component_string[comp_id];
}

static std::string logging_level_string[N_LOGGING_LEVEL] = {
	"FATAL",
	"ERROR",
	"WARN",
	"INFO",
	"DEBUG",
};

const std::string& strlogginglevel(logging_level_t level) {
	return logging_level_string[level];
}

std::string debug_format_event(
	logging_level_t event_level,
	component_t comp_id,
	int _errno,
	std::string msg,
	const char *file,
	const char *lineno) {

	std::ostringstream oss;
	oss << "[" << strlogginglevel(event_level) << "] ";
	oss << file << ":" << lineno << ":";
	oss << strcomponent(comp_id) << ":" << _errno << ": ";
	oss << msg;

	return oss.str();
}

std::string info_format_event(
	logging_level_t event_level,
	component_t comp_id,
	int _errno,
	std::string msg,
	const char *file,
	const char *lineno) {

	std::ostringstream oss;
	oss << "[" << strlogginglevel(event_level) << "] ";
	oss << msg;
	return oss.str(); 
}

static std::list<std::unique_ptr<event_logger>> logger_list;

void add_logger(std::unique_ptr<event_logger> p_logger) {
	logger_list.push_back(std::move(p_logger));
}

void log_event(
	logging_level_t event_level,
	component_t comp_id,
	int _errno,
	std::string msg,
	const char *file,
	const char *lineno) {
	
	for (const auto& plogger : logger_list) {
		plogger->log_event(event_level, comp_id, _errno, msg, file, lineno);
	}
}

class file_event_logger: public event_logger {
public:
	
	file_event_logger(
			logging_level_t logging_level,
			FILE *fp, bool should_close)
		: event_logger(logging_level), fp(fp), should_close(should_close) {}

	~file_event_logger() {
		if (should_close)
			fclose(fp);
	}

	void log_event(
		logging_level_t event_level,
		component_t comp_id,
		int _errno,
		std::string msg,
		const char *file,
		const char *lineno) {
		
		if (logging_level() >= event_level) {
			std::string &&m = get_default_logging_string(
				event_level, comp_id, _errno, std::move(msg), file, lineno);
			fprintf(fp, "%s\n", m.c_str());
		}
	}

private:
	FILE *fp;
	bool should_close;
};

void add_file_logger(
		logging_level_t logging_level,
		const char *filename, const char *mode) {
	
	FILE *fp = fopen(filename, mode);
	if (fp) {
		add_logger(std::unique_ptr<event_logger>
				(new file_event_logger(logging_level, fp, true)));
	}
}

void add_file_logger(
		logging_level_t logging_level,
		FILE *fp) {
	if (fp)
		add_logger(std::unique_ptr<event_logger>
				(new file_event_logger(logging_level, fp, false)));
}

void clear_loggers() { 
	logger_list.clear();	
}


