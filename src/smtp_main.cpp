#include "config.h"

#include "errs.h"
#include "smtp_client.h"
#include "utils.h"

#include <iostream>
#include <fstream>
#include <cstring>
using std::cin;
using std::cout;
using std::endl;
using std::string;
using std::getline;

static const auto npos = std::string::npos;

void smtp_main_usage(const char *progname) {
	cout << "usage: " << progname << " [infile]"
		" [server] [port] [mail_addr] [password]" << endl;
}

bool try_parse_header(const std::string &input,
		std::string &field, std::string &value) {
	auto i = input.find(":");
	if (i == npos) return false;
	
	field = input.substr(0, i);
	if (i + 1 < input.length())
		value = input.substr(i+1, npos);
	else
		value.clear();
	return true;
}

void smtp_main_get_input(std::istream& in, mail_t &mail) {
	if (!in) 
		throw_error(COMP_MAIN, "cannot read from input");
	
	bool has_subject = false;
	string line;
	string field, value;

	auto add_field = [&](const string& value, 
			typename std::string::size_type i = 0) {
		
		if (ci_eq_str("from", field.begin(), field.end())) {
			std::string name, addr;
			i = get_next_name_email(value, i, name, addr);
			if (i == npos) return ;
			
			mail.set_from(get_formated_addr(name, addr));
		}
		else if (ci_eq_str("to", field.begin(), field.end())) {
			while (i != value.length()) {
				std::string name, addr;
				i = get_next_name_email(value, i, name, addr);
				if (i == npos) return ;
				
				mail.add_to(get_formated_addr(name, addr));
			}
		}
		else if (ci_eq_str("cc", field.begin(), field.end())) {
			while (i != value.length()) {
				std::string name, addr;
				i = get_next_name_email(value, i, name, addr);
				if (i == npos) return;

				mail.add_cc(get_formated_addr(name, addr));
			}
		}
		else if (ci_eq_str("subject", field.begin(), field.end())) {
			has_subject = true;
			mail.set_subject(value.substr(i, npos));
		}
		else if (ci_eq_str("date", field.begin(), field.end())) {
			mail.set_date(value.substr(i, npos));	
		}
		else {
			mail.append_field(field, value.substr(i, npos));
		}
	};

	log_debug(COMP_MAIN, "proc header");
	if (getline(in, line), !in)
		goto ask_for_headers;
	if (line.empty() || !try_parse_header(line, field, value)) {
		goto proc_text;	
	}
	
	add_field(value);
	while (getline(in, line) && in && !line.empty()) {
		if (line[0] == ' ' || line[0] == '\t') {
			add_field(line, 1);
		}
		else {
			if (!try_parse_header(line, field, value)) {
				throw_error_f(COMP_MAIN, "invalid header line: %s",
						line.c_str());
			}
			add_field(value);
		}
	}
	
proc_text:
	log_debug(COMP_MAIN, "proc text");
	if (line.empty()) {
		if (getline(in, line), !in)
			goto ask_for_headers;
		mail.text_append_line(line);
	}

	while (getline(in, line), in)
		mail.text_append_line(line);

ask_for_headers:
	cin.clear();

	if (!has_subject) {
		line = get_input("Subject");
		if (!line.empty())
			mail.set_subject(line);
	}
	
	while (!(line = get_input("To")).empty()) {
		field = "to";
		try {
			add_field(line);
		}
		catch(...) {}
	}
	while (!(line = get_input("cc")).empty()) {
		field = "cc";
		try {
			add_field(line);
		}
		catch (...) {}
	}
}


int main(int argc, char *argv[]) {
	if (argc > 1 && (
			(!strcmp("-h", argv[1])) ||
			(!strcmp("--help", argv[1])))) {
		smtp_main_usage(argv[0]);
		return 0;
	}

	init_default_event_loggers(LLEVEL_INFO, nullptr);
	
	std::string infile = (argc > 1) ? argv[1] : "-";
	std::string server = (argc > 2) ? argv[2] : get_input("server");
	std::string port = (argc > 3) ? argv[3] : get_input("port");
	std::string mail_addr = (argc > 4) ? argv[4] : get_input("mail_addr");
	std::string pass = (argc > 5) ? argv[5] : get_pass("password");
	
try {
	mail_t mail;
	if (infile == "-") {
		smtp_main_get_input(cin, mail);	
	}
	else {
		std::ifstream fin(infile);
		smtp_main_get_input(fin, mail);
	}

	smtp_client_t smtp(server, port, mail_addr, pass);
	smtp.send_mail(mail);
}
catch (...) {
	std::cout << "error" << std::endl;
}
	return 0;
}

