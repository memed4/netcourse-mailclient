#include "config.h"

#include "errs.h"
#include "client_socket.h"
#include "smtp_client.h"
#include "utils.h"
#include "encoding.h"

#include <sstream>
#include <iterator>
#include <algorithm>

smtp_client_t::smtp_client_t(std::string server,
		std::string port,
		std::string email,
		std::string pass)
	: server(server), port(port), pass(pass) {

	auto i = email.find("@");
	if (i == std::string::npos) {
		throw_error_f(COMP_SMTP, "invalid email address: %s", 
				email.c_str());
	}
	user = email.substr(0, i);
	domain = email.substr(i + 1, std::string::npos);
}

void smtp_client_t::send_mail(mail_t& mail) {
	/* check header and set mail_from, mail_to */
	check_mail(mail);
	
	/* establish connection */
	client_socket_t con(connection_t::EOLN_CRLF);	
	con.init_conn(server, port);

	try {
		/* receive greeting */
		recv_greeting(con);
		
		/* try ehlo; if ehlo is not supported by the server,
		 * abort. */
		ehlo(con);
		
		/* authentication */
		log_debug_f(COMP_SMTP, "auth support:%s%s", 
				(support_login) ? " login" : "",
				(support_plain) ? " plain" : "");
		if (support_login) {
			auth_login(con);
		}
		else if (support_plain) {
			auth_plain(con);
		}
		else {
			log_warn(COMP_SMTP, "no authentication support");
		}
	
		/* mail from, to */	
		mail_from(con);
		mail_to(con);
		
		/* send data */
		mail_data(con, mail);
	} catch (const std::exception& e) {
		quit_early(con);
		throw e;
	}

	quit(con);

	log_info(COMP_SMTP, "mail is successfully sent");
}

void smtp_client_t::check_mail(mail_t& mail) {
	log_debug(COMP_SMTP, "check mail");
	std::string s;
	if (!mail.get_date(s)) {
		mail.set_date();
		mail.get_date(s);
	}
	log_debug_f(COMP_SMTP, "Date: %s", s.c_str());
	
	if (!mail.get_from(s)) {
		_mail_from = "<";
		_mail_from.append(user).append("@").append(domain)
			.append(">");
		s = "\"";
		s.append(user).append("\"").append(_mail_from);
		mail.set_from(s);
	}
	else {
		std::string name_part;
		std::string addr;
		auto i = get_next_name_email(s, 0, name_part, addr);
		if (i == std::string::npos) {
			name_part = "";
			addr = user + "@" + domain;
			mail.set_from(get_formated_addr(name_part, addr));
		}
		else if (addr.empty()) {
			addr = user + "@" + domain;
			mail.set_from(get_formated_addr(name_part, addr));
		}
		(_mail_from = "<").append(addr).append(">");
	}
	log_debug_f(COMP_SMTP, "From: %s", s.c_str());

	std::vector<std::string> to;

	if (mail.get_to(to)) {
	}
	else {
		throw_error(COMP_SMTP, "no recipient");
	}

	decltype(to.size()) i;
	for (i = 0; i < to.size(); ++i) {
		if (!check_recipient_format(to[i], s)) {
			throw_error_f(COMP_SMTP, "invalid recipient: %s", 
					to[i].c_str());				
		}
		_mail_to.push_back(s);
		log_debug_f(COMP_SMTP, "To: %s", to[i].c_str());
	}

	mail.get_cc(to);
	for ( ; i < to.size(); ++i) {
		if (!check_recipient_format(to[i], s)) {
			throw_error_f(COMP_SMTP, "invalid recipient: %s", 
					to[i].c_str());				
		}
		_mail_to.push_back(s);
		log_debug_f(COMP_SMTP, "cc: %s", to[i].c_str());
	}

}

bool smtp_client_t::check_recipient_format(const std::string& s, std::string& addr_part) {
	std::string name_part;
	std::string addr;
	auto i = get_next_name_email(s, 0, name_part, addr);
	addr_part = "<";
	addr_part.append(addr).append(">");
	return i != std::string::npos;
}

void smtp_client_t::recv_greeting(connection_t& con) {
	std::string msg = con.readline();
	if (!start_with(msg, 220)) {
		throw_error_f(COMP_SMTP, "%s", msg.c_str());
	}
}

void smtp_client_t::ehlo(connection_t& con) {
	std::string hostaddr;
	if (!con.get_local_name(&hostaddr, 0)) {
		hostaddr = "unknown";
	}

	std::string msg = "EHLO ";
	msg.append(hostaddr);
	con.writeline(msg);
	
	do {
		msg = con.readline();
		if (!start_with(msg, 250)) {
			throw_error_f(COMP_SMTP, "%s", msg.c_str());
		}
		if (msg.length() > 4) {
			std::istringstream is(msg.substr(4, std::string::npos));
			std::string ext;
			if (is >> ext) {
				/* ignore extensions other than AUTH */
				if (ext != "AUTH") continue;
			}
			else {
				continue;
			}

			while (is >> ext) {
				if (ext == "LOGIN") support_login = true;
				else if (ext == "PLAIN") support_plain = true;
				else {
					log_warn_f(COMP_SMTP, "unsupported AUTH: %s",
							ext.c_str());
				}
			}
		}
	} while (has_next_line(msg));
}

void smtp_client_t::auth_login(connection_t& con) {
	std::string email = user + "@" + domain;
	std::string encoded_user;
	std::string encoded_pass;

	base64_encode(email.begin(), email.end(), 
			std::back_inserter(encoded_user));
	base64_encode(pass.begin(), pass.end(),
			std::back_inserter(encoded_pass));
	
	log_debug(COMP_SMTP, "auth login");
	std::string msg = "AUTH LOGIN";
	con.writeline(msg);

	msg = con.readline();
	if (!start_with(msg, 334)) {
		throw_error_f(COMP_SMTP, "auth failed: %s", msg.c_str());	
	}
	
	con.writeline(encoded_user);
	msg = con.readline();
	if (!start_with(msg, 334)) {
		throw_error_f(COMP_SMTP, "auth failed: %s", msg.c_str());
	}

	con.writeline(encoded_pass);
	msg = con.readline();
	if (!start_with(msg, 235)) {
		throw_error_f(COMP_SMTP, "auth failed: %s", msg.c_str());
	}
}

void smtp_client_t::auth_plain(connection_t& con) {
	std::vector<char> raw;
	raw.reserve(user.length() * 2 + pass.length() + 2);
	std::copy(user.begin(), user.end(), std::back_inserter(raw));
	raw.push_back('\0');
	std::copy(user.begin(), user.end(), std::back_inserter(raw));
	raw.push_back('\0');
	std::copy(pass.begin(), pass.end(), std::back_inserter(raw));

	std::string encoded;
	base64_encode(raw.begin(), raw.end(), 
			std::back_inserter(encoded));

	log_debug(COMP_SMTP, "auth plain");
	std::string msg = "AUTH PLAIN";
	con.writeline(msg);

	msg = con.readline();
	if (!start_with(msg, 334)) {
		throw_error_f(COMP_SMTP, "auth failed: %s", msg.c_str());
	}
	
	con.writeline(encoded);
	msg = con.readline();
	if (!start_with(msg, 235)) {
		throw_error_f(COMP_SMTP, "auth failed: %s", msg.c_str());
	}
}

void smtp_client_t::mail_from(connection_t& con) {
	log_debug(COMP_SMTP, "mail from");
	std::string msg = "MAIL FROM: ";
	msg.append(_mail_from);
	con.writeline(msg);

	msg = con.readline();
	if (!start_with(msg, 250)) {
		throw_error_f(COMP_SMTP, "mail from: %s: %s", 
				_mail_from.c_str(), msg.c_str());	
	}
}

void smtp_client_t::mail_to(connection_t& con) {
	log_debug(COMP_SMTP, "rcpt to");
	for (const auto &i : _mail_to) {
		std::string msg = "RCPT TO: ";
		msg.append(i);
		con.writeline(msg);
		msg = con.readline();
		if (!start_with(msg, 250)) {
			throw_error_f(COMP_SMTP, "mail to: %s: %s",
				i.c_str(), msg.c_str());
		}
	}
}

void smtp_client_t::mail_data(connection_t& con, mail_t &mail) {
	std::string text;
	mail.get_mail_body(text);

	log_debug(COMP_SMTP, "data");

	std::string msg = "DATA";
	con.writeline(msg);
	
	msg = con.readline();
	if (!start_with(msg, 354)) {
		throw_error_f(COMP_SMTP, "data: %s", msg.c_str());
	}
	
	con.writeline(text);	
	msg = con.readline();
	if (!start_with(msg, 250)) {
		throw_error_f(COMP_SMTP, "data: %s", msg.c_str());
	}
}

void smtp_client_t::quit_early(connection_t& con) noexcept {
	try {
		std::string msg = "QUIT";
		con.writeline(msg);
	} catch(...) {}
}

void smtp_client_t::quit(connection_t& con) {
	log_debug(COMP_SMTP, "quit");
	std::string msg = "QUIT";
	con.writeline(msg);
	msg = con.readline();
	if (!start_with(msg, 221)) {
		throw_error_f(COMP_SMTP, "quit: %s", msg.c_str());
	}
}



