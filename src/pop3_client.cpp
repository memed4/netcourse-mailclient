#include "config.h"

#include "pop3_client.h"
#include "errs.h"
#include "client_socket.h"
#include <unistd.h>
#include <vector>
#include <fstream>
#include <cstdlib>

void pop3_client_t::recv_all(bool del) {
	recv_all(std::cout, del);
}

void pop3_client_t::recv_all(std::string filename, bool del) {
	std::ofstream ofs(filename);
	recv_all(ofs, del);
	ofs.close();
}

void pop3_client_t::recv_all(std::ostream& os, bool del) {
	
	client_socket_t con(connection_t::EOLN_CRLF);
	std::vector<mail_info_t> v;
	std::vector<std::string> msgs;

	/* establish TCP connection */
	log_info_f(COMP_POP3, "connecting to %s:%s", 
			server.c_str(), port.c_str());
	con.init_conn(server, port);
	log_info_f(COMP_POP3, "connection established");
	
	try {
		/* receive greeting */
		recv_greeting(con);
	
		/* authorization state */
		do_auth(con);
		
		/* retrieve messages */
		list(con, v);
		
		log_info_f(COMP_POP3, "%u msgs in the mail box", (unsigned)  v.size());
		
		for (auto i = v.begin(); i != v.end(); ++i) {
			log_info_f(COMP_POP3, "retrieving %d", i->no);
			msgs.push_back(retr(con, i->no));
			if (del) {
				log_info_f(COMP_POP3, "deleting %d", i->no);
				dele(con, i->no);
			}
		}
	} catch(const std::exception& e) {
		quit_early(con);
		throw e;
	}

	log_info(COMP_POP3, "quit");
	quit(con);

	con.close();
	
	log_info(COMP_POP3, "writing to output");
	os << v.size() << " msgs in total" << std::endl;

	for (decltype(msgs.size()) i = 0; i != msgs.size(); ++i) {
		os << std::endl;
		os << "msg " << v[i].no << " len = " << v[i].len 
			<< std::endl;
		os << "------------------BEGIN-MESSAGE--------------------"
			<< std::endl;
		os << msgs[i];
		os << "--------------------END-MESSAGE--------------------"
			<< std::endl;
	}
}

void pop3_client_t::recv_greeting(connection_t& con) {
	std::string msg = con.readline();
	
	if (is_ok(msg)) {
		return ;	
	}
	throw_error_f(COMP_POP3, "server error: %s", msg.c_str());
}

void pop3_client_t::do_auth(connection_t& con) {
	std::string msg;

	msg = "USER " + user;
	con.writeline(msg);

	msg = con.readline();
	if (!is_ok(msg)) {
		throw_error_f(COMP_POP3, "auth error: %s", msg.c_str());
	}

	msg = "PASS " + pass;
	con.writeline(msg);

	msg = con.readline();
	if (!is_ok(msg)) {
		throw_error_f(COMP_POP3, "auth error: %s", msg.c_str());
	}
}

int pop3_client_t::stat(connection_t& con) {
	std::string msg = "STAT";
	con.writeline(msg);

	msg = con.readline();
	if (!is_ok(msg)) {
		throw_error_f(COMP_POP3, "stat error: %s", msg.c_str());
	}

	int n = atoi(msg.c_str() + 3);
	if (n < 0) {
		throw_error_f(COMP_POP3, 
				"stat error: negative number of mails %d", n);
	}
	return n;
}

void pop3_client_t::list(connection_t& con,
		std::vector<mail_info_t>& v) {
	std::string msg = "LIST";
	con.writeline(msg);

	msg = con.readline();
	if (!is_ok(msg)) {
		throw_error_f(COMP_POP3, "list error: %s", msg.c_str());
	}
	
	mail_info_t minfo;
	while ((msg = con.readline()) != ".") {
		if (sscanf(msg.c_str(), "%d%d", &minfo.no, &minfo.len) != 2) {
			throw_error_f(COMP_POP3, "list parsing error: %s", msg.c_str());
		}
		v.push_back(minfo);
	}
}

void pop3_client_t::list(connection_t& con,
		int msg_no, std::vector<mail_info_t>& v) {
	
	std::string msg = "LIST " + to_string(msg_no);
	con.writeline(msg);

	msg = con.readline();
	if (!is_ok(msg)) {
		throw_error_f(COMP_POP3, "list error: %s", msg.c_str());
	}
	
	mail_info_t minfo;
	if (sscanf(msg.c_str() + 3, "%d%d", &minfo.no, &minfo.len) != 2) {
		throw_error_f(COMP_POP3, "list parsing error: %s", msg.c_str());
	}
	v.push_back(minfo);
}

std::string pop3_client_t::retr(connection_t& con, int msg_no) {
	std::string msg = "RETR " + to_string(msg_no);
	con.writeline(msg);

	msg = con.readline();
	if (!is_ok(msg)) {
		throw_error_f(COMP_POP3, "retr error: %s", msg.c_str());
	}
	
	std::string mail;
	int nchar;
	if (sscanf(msg.c_str() + 3, "%d", &nchar) == 1) {
		mail.reserve(nchar);	
	}
	else {
		mail.reserve(DEFAULT_BUFFER_SIZE);
	}
	
	while ((msg = con.readline()) != ".") {
		if (msg.length() >= 2 && msg[0] == '.' && msg[1] == '.') {
			mail.append(msg.begin() + 1, msg.end());
		}
		else {
			mail.append(msg);
		}
		mail.append("\n");
	}

	return std::move(mail);
}

void pop3_client_t::dele(connection_t& con, int msg_no) {
	std::string msg = "DELE " + to_string(msg_no);
	con.writeline(msg);

	msg = con.readline();
	if (!is_ok(msg)) {
		throw_error_f(COMP_POP3, "dele error: %s", msg.c_str());
	}
}

void pop3_client_t::quit_early(connection_t& con) noexcept {
	std::string msg = "QUIT";
	con.writeline(msg);
}

void pop3_client_t::quit(connection_t& con) {
	std::string msg = "QUIT";
	con.writeline(msg);

	msg = con.readline();
	if (!is_ok(msg)) {
		throw_error_f(COMP_POP3, "quit error: %s", msg.c_str());
	}
}

