#include "utils.h"
#include "mail.h"
#include "errs.h"

#include <cstdio>
#include <cstdarg>
#include <utility>
#include <string>
#include <iostream>
#include <unistd.h>
#include <utility>
#include <ctime>

static std::string format_string(const char *format, va_list args) {
	char buffer[DEFAULT_FSTRING_BUF_SIZE];
	int n = vsnprintf(buffer, DEFAULT_FSTRING_BUF_SIZE, format, args);
	if (n < 0) {
		va_end(args);
		throw_error(COMP_OTHER, "format string failed");
	}
	if (n < DEFAULT_FSTRING_BUF_SIZE) {
		va_end(args);
		return std::string(buffer, buffer + n);
	}

	log_warn(COMP_OTHER, "format_string buffer overflow");

	try {
		char *p_buf = new char[n + 1];
		if (vsnprintf(buffer, n + 1, format, args) != n) {
			va_end(args);
			delete [] p_buf;
			throw_error(COMP_OTHER, "format string failed");
		}

		std::string ret(p_buf, p_buf + n);

		va_end(args);
		delete [] p_buf;
		return std::move(ret);
	} catch (std::bad_alloc) {
		va_end(args);
		throw_error(COMP_OTHER, "out of memory");
	}
}

std::string format_string(const char *format, ...) {
	va_list args;
	va_start(args, format);
	return format_string(format, args);
}

std::string format_string(const char *default_string,
		const char *format, ...) noexcept {
	va_list args;
	va_start(args, format);

	try {
		return format_string(format, args);
	} catch(...) {
		return default_string;
	}
}

std::string get_input(std::string prompt) {
	std::string in;

	std::cout << prompt << ": ";
	std::getline(std::cin, in);
	return std::move(in);
}

std::string get_pass(std::string prompt) {
	prompt += ": ";
	return getpass(prompt.c_str());	
}

std::string get_local_time_str() {
	char buffer[100];
	time_t t = time(nullptr);
	strftime(buffer, 100, "%a, %d %b %Y %H:%M:%S %z", localtime(&t));
	return std::string(buffer);
}

typename std::string::size_type
get_next_name_email(const std::string &v,
		std::string::size_type i, 
		std::string &name, 
		std::string &mail_addr) {

	skip_sp(v, i);
	if (i == v.length()) return std::string::npos;
	if (v[i] == ',') {
		++i;
	}
	skip_sp(v, i);
	if (i == v.length()) return std::string::npos;
	
	/* optional name part */
	if (v[i] == '"') {
		++i;
		while (v[i] != '"') {
			if (v[i] == '\\') {
				if (++i == v.length())
					return std::string::npos;
				name.push_back('\\');
				name.push_back(v[i]);
			}
			else {
				name.push_back(v[i]);
			}

			if (++i == v.length())
				throw_error_f(COMP_OTHER, 
					"invalid format of mail address: %s",
					v.c_str());
		}
		++i;
	}
	else {
		while (!is_special(v[i]) && !is_sp(v[i])) {
			name.push_back(v[i]);	
			if (++i == v.length())
				throw_error_f(COMP_OTHER, 
					"invalid format of mail address: %s",
					v.c_str());
		}
	}

	skip_sp(v, i);

	/* mail addr part */
	auto is_eof_addr = (v[i] == '<') ?
		(++i, [](char x) -> bool { return x == '>'; }) :
		[](char x) -> bool { return is_sp(x); };
	
	if (i == v.length() ) {
		throw_error_f(COMP_OTHER, 
			"invalid format of mail address: %s",
			v.c_str());
	}
	while (!is_eof_addr(v[i])) {
		mail_addr.push_back(v[i]);
		if (++i == v.length())
			throw_error_f(COMP_OTHER, 
				"invalid format of mail address: %s",
				v.c_str());
	}

	return (v[i] == '>') ? (i+1) : i;
}
	
