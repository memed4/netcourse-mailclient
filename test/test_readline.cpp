#include "config.h"

#include "errs.h"
#include "connection.h"

#include <string>
#include <utility>
#include <algorithm>
#include <cstring>

class str_connection_t : public connection_t {
public:
	str_connection_t(std::string contents, EOLN_style eoln_style,
			int buf_size = DEFAULT_BUFFER_SIZE) :
		connection_t(eoln_style, buf_size), contents(std::move(contents)),
		content_head(0), next_error(false) {}
	
protected:
	ssize_t write_no_buf(const char *buffer, size_t len) noexcept {
		/* does nothing */ 
		return len;
	}

	ssize_t read_no_buf(char *buffer, size_t buf_size) noexcept {
		if (next_error) return -1;

		ssize_t len = std::min(contents.size() - content_head, buf_size);
		std::copy(contents.c_str() + content_head,
				contents.c_str() + content_head + len,
				buffer);
		content_head += len;
		return len;
	}

public:

	const std::string contents;
	ssize_t content_head;
	bool next_error;

	void reset_head() {
		content_head = 0;
	}

	void set_error() {
		next_error = true;	
	}

	void clear_error() {
		next_error = false;
	}
};

constexpr int con_buf_size = 32;
const char *msgs[] = {
"SHORT LINE",
"LONG LINE LONG LINE LONG LINE LONG LINE",
"EMPTY",
"LINE WITH \r and \n\r and \r",
"LINE WITH FIRST R AT THE    END",
"AM I OK?",
"LINE WITH RN AT THE END      D",
/* start very long line */
"LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"
"LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"
"LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"
"LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"
"LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL"
,/* end very long line*/
"AM I OK TOO?",
"",
"AM I TOO OK?",
};
constexpr int nmsgs = sizeof(msgs) / sizeof(*msgs);

constexpr int expecting_io_no = 7;

std::string recv_msgs;
void init_recv_msgs() {
	recv_msgs.clear();
	for (int i = 0; i < nmsgs; ++i) {
		recv_msgs.append(msgs[i]);
		recv_msgs.append("\r\n");
	}
}

bool fail = false;

bool read_and_check_msg(connection_t &conn, int i) {
	try {
		std::string &&s = conn.readline();
		if (s != msgs[i]) {
			log_warn_f(COMP_OTHER, "msg mismatch:\n[should be]\n%s\n[but got]\n%s", msgs[i], s.c_str());
			fail = true;
			return false;
		}
	} catch (...) {
		log_warn(COMP_OTHER, "unexpected error");
		fail = true;
		return false;
	}
	return true;
}

bool check_end(connection_t &conn) {
	std::string s;
	try {
		conn.readline(s);
		log_warn(COMP_OTHER, "expecting eof");
		fail = true;
		return false;
	
	} catch(...) {
		log_info(COMP_OTHER, "caught eof as expected");
		return true;
	}
}

int main() {
	init_default_event_loggers(LLEVEL_INFO, nullptr);

	init_recv_msgs();
	str_connection_t conn(recv_msgs, connection_t::EOLN_CRLF, con_buf_size);

//test1:
{
	log_info(COMP_OTHER, "test readline with no io errors");
	for (int i = 0; i < nmsgs; ++i) {
		if (!read_and_check_msg(conn, i)) goto test2;
	}
	check_end(conn);
}

test2: {
	conn.reset_head();
	log_info(COMP_OTHER, "test io error");
	for (int i = 0; i < expecting_io_no; ++i) {
		if (!read_and_check_msg(conn, i)) goto test3;
	}
	conn.set_error();
	try {
		std::string &&s = conn.readline();
		fail = true;
		log_warn(COMP_OTHER, "io error is not caught as expected");
		goto test3;
	}
	catch (...) {
		log_info(COMP_OTHER, "io error caught as expected");
	}
	conn.clear_error();
	try {
		conn.readline();
	}
	catch (...) {
		fail = true;
		log_warn(COMP_OTHER, "unexpected error");
		goto test3;

	}
	for (int i = expecting_io_no + 1; i < nmsgs; ++i) {
		if (!read_and_check_msg(conn, i)) goto test3;
	}
	check_end(conn);
}

test3: {
	conn.reset_head();
	conn.clear_error();
	log_info(COMP_OTHER, "test error recovery");
	for (int i = 0; i < expecting_io_no; ++i) {
		if (!read_and_check_msg(conn, i)) goto test4;
	}
	conn.set_error();
	std::string test3_line;
	try {
		conn.readline(test3_line);
		fail = true;
		log_warn(COMP_OTHER, "io error is not caught as expected");
		goto test4;
	}
	catch (...) {
		log_info(COMP_OTHER, "io error caught as expected");
	}
	conn.clear_error();
	try {
		conn.readline(test3_line);
	}
	catch (...) {
		fail = true;
		log_warn(COMP_OTHER, "unexpected error");
		goto test4;

	}
	if (test3_line != msgs[expecting_io_no]) {
		log_warn_f(COMP_OTHER, "msg mismatch:\n[should be]\n%s\n[but got]\n%s", 
				msgs[expecting_io_no], test3_line.c_str());
		fail = true;
		goto test4;
	}
	for (int i = expecting_io_no + 1; i < nmsgs; ++i) {
		if (!read_and_check_msg(conn, i)) goto test4;
	}
	check_end(conn);
}

test4: {
	conn.reset_head();
	conn.clear_error();
	log_info(COMP_OTHER, "test interleaved read and readline");
	if (!read_and_check_msg(conn, 0)) goto end;
	auto test4_start = strlen(msgs[0]) + conn.get_eoln_len();
	auto test4_end = recv_msgs.find("WITH RN", test4_start);
	auto test4_buf_len = test4_end - test4_start;
	std::string test4_str;
	test4_str.resize(test4_buf_len);
	ssize_t test4_n_read = conn.read(&*test4_str.begin(), test4_buf_len);
	if (test4_n_read != (ssize_t) test4_buf_len) {
		log_warn_f(COMP_OTHER, "length incorrect, should be %llu, but got %llu", (unsigned long long) test4_buf_len, (unsigned long long) test4_n_read);
		fail = true;
		goto end;
	}
	if (test4_str != recv_msgs.substr(test4_start, test4_buf_len)) {
		log_warn_f(COMP_OTHER, "read incorrect content\n"
		"[should be]\n%s\n[but got]\n%s\n[end]", 
		recv_msgs.substr(test4_start, test4_buf_len).c_str(),
		test4_str.c_str());
		fail = true;
		goto end;
	}
	
	test4_str.clear();
	auto test4_eoln_p = recv_msgs.find(conn.get_eoln(), test4_end);
	try {
		std::string &&m = conn.readline();
		if (m != recv_msgs.substr(test4_end, test4_eoln_p - test4_end)) {
			log_warn_f(COMP_OTHER, "remaining content on this line is incorrect\n[should be]\n%s\n[but got]\n%s",
				recv_msgs.substr(test4_end, test4_eoln_p - test4_end).c_str(),
				m.c_str());
			fail = true;
			goto end;
		}
	}
	catch (...) {
		log_warn(COMP_OTHER, "unexpected error");
	}
	for (int i = expecting_io_no; i < nmsgs; ++i) {
		if (!read_and_check_msg(conn, i)) goto end;
	}
	check_end(conn);
}
end:
	return fail ? 1 : 0;
}

