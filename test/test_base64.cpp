#include "config.h"

#include "errs.h"
#include "encoding.h"

#include <iterator>

bool test(std::string &&original, std::string &&encoded) {
	std::string my_encoded;
	
	base64_encode(original.begin(), original.end(), std::back_inserter(my_encoded));

	log_info(COMP_OTHER, "test base64");
	log_info_f(COMP_OTHER, "original: %s", original.c_str());
	log_info_f(COMP_OTHER, "correctly encoded: %s", encoded.c_str());
	log_info_f(COMP_OTHER, "encoded: %s", my_encoded.c_str());

	return encoded == my_encoded;
}

int main() {
	init_default_event_loggers(LLEVEL_INFO, nullptr);

	if (!test("abcdefghi", "YWJjZGVmZ2hp")) return 1;
	if (!test("jklmnopq", "amtsbW5vcHE=")) return 1;
	if (!test("rstuvwx", "cnN0dXZ3eA==")) return 1;

	return 0;
}
