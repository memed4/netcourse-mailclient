#ifndef LOGGING_LEVEL
#	error "define LOGGING_LEVEL to compile this test"
#endif

#include "errs.h"

#include <string>
#include <fstream>
#include <utility>
#include <iterator>
#include <memory>

#include <unistd.h>

class loopback_logger : public event_logger {
public:
	loopback_logger(logging_level_t level):
		event_logger(level) {}
	
	void log_event(
			logging_level_t event_level,
			component_t comp_id,
			int _errno,
			std::string msg,
			const char *file,
			const char *lineno) {
		if (logging_level() >= event_level) {
			c.append(get_default_logging_string(
				event_level, comp_id, _errno, std::move(msg), file, lineno) + "\n");
		}
	}


	std::string c;
};

bool compare_with_file(const std::string& c, const char *filename) {
	std::ifstream fin(filename);
	char buf[c.size() + 1];
	buf[c.size()] = '\0';
	fin.read(buf, c.size());
	return c == buf && fin.get() == std::ifstream::traits_type::eof();
}

int main(int argc, const char *argv[]) {
	if (argc <= 1) {
		printf("usage: %s <tmpfilename>\n", argv[0]);
	}
	std::string tmp_name = argv[1];
	init_default_event_loggers(LOGGING_LEVEL, tmp_name.c_str());

	loopback_logger *lb_logger = new loopback_logger(LOGGING_LEVEL);
	add_logger(std::unique_ptr<event_logger>(lb_logger));
	
	log_debug(COMP_OTHER, "test log_debug");
	log_debug_f(COMP_OTHER, "test %s", "log_debug_f");

	log_info(COMP_OTHER, "test log_info");
	log_info_f(COMP_OTHER, "test %s", "log_info_f");

	log_warn(COMP_OTHER, "test log_warn");
	log_warn_f(COMP_OTHER, "test %s", "log_warn_f");

	try {
		throw_error(COMP_OTHER, "test throw_error");
	} catch(const exception_base& e) {
		log_info_f(COMP_OTHER, "error caught");
	}

	try {
		throw_error_f(COMP_OTHER, "test %s", "throw_error_f");
	} catch(const exception_base& e) {
		log_info_f(COMP_OTHER, "error caught");
	}

	try {
		throw_fatal(COMP_OTHER, "test throw_fatal");
	} catch(const exception_base& e) {
		log_info_f(COMP_OTHER, "fatal error caught");
	}

	try {
		throw_fatal_f(COMP_OTHER, "test %s", "throw_fatal_f");
	} catch(const exception_base& e) {
		log_info_f(COMP_OTHER, "fatal error caught");
	}

	std::string c = std::move(lb_logger->c);
	clear_loggers();

	bool success = compare_with_file(c, tmp_name.c_str());
	if (argc == 1) unlink(tmp_name.c_str());		
	
	if (!success) {
		fprintf(stderr, "output mismatch\n[lb]\n%s\n", c.c_str());
	}

	return success ? 0 : 1;
}
