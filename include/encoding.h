#ifndef AMC_ENCODING_H
#define AMC_ENCODING_H

#include "config.h"
#include <vector>
#include <string>
#include <sys/types.h>

extern "C" {
	extern char base64_encoding_table[64];
	extern unsigned char base64_decoding_table[256];
}

template<class InputIterator, class OutputIterator>
void base64_encode(InputIterator ibegin, InputIterator iend, OutputIterator iout) {
#define base64_ref(i) base64_encoding_table[(unsigned) (i)]
#define base64_get_1 base64_ref(b[0] >> 2)
#define base64_get_2 base64_ref(((b[0] & 3) << 4) | (b[1] >> 4))
#define base64_get_3 base64_ref(((b[1] & 0xf) << 2) | (b[2] >> 6))
#define base64_get_4 base64_ref((b[2] & 0x3f))

	unsigned char b[3];
	int i = 0;
	while (ibegin != iend) {
		b[i] = *ibegin++;
		if (++i == 3) {
			i = 0;
			*iout++ = base64_get_1;
			*iout++ = base64_get_2;
			*iout++ = base64_get_3;
			*iout++ = base64_get_4;
		}
	}

	if (i == 1) {
		b[1] = 0;
		*iout++ = base64_get_1;
		*iout++ = base64_get_2;
		*iout++ = '=';
		*iout++ = '=';
	}

	else if (i == 2) {
		b[2] = 0;
		*iout++ = base64_get_1;
		*iout++ = base64_get_2;
		*iout++ = base64_get_3;
		*iout++ = '=';
	}

#undef base64_get_4
#undef base64_get_3
#undef base64_get_2
#undef base64_get_1
#undef base64_ref
}

template<class InputIterator, class OutputIterator>
bool base64_decode(InputIterator ibegin, InputIterator iend, OutputIterator iout) {
#define base64_ref(i) base64_encoding_table[(unsigned) (i)]
#define base64_get_1 (base64_ref(b[0]) << 2) | (base64_ref(b[1]) >> 4)
#define base64_get_2 (((base64_ref(b[1]) & 0xf) << 4) | (base64_ref(b[2]) >> 2))
#define base64_get_3 (((base64_ref(b[2]) & 0x3) << 6) | base64_ref(b[3]))
	unsigned char b[4];
	int i = 0;
	while (ibegin != iend) {
		unsigned char b[i] = *ibegin++;
		if (++i == 3) {
			i = 0;
			if (b[0] != 'A' && !base64_ref(b[0])) return false;
			if (b[1] != 'A' && !base64_ref(b[1])) return false;

			if (b[2] != '=') {
				if (b[2] != 'A' && !base64_ref(b[2])) return false;
				if (b[3] != '=') {
					if (b[3] != 'A' && !base64_ref(b[3])) return false;	
					/* 3n bytes */
					*iout++ = base64_get_1;
					*iout++ = base64_get_2;
					*iout++ = base64_get_3;
				}
				else {
					if (ibegin != iend) return false;
					/* 3n + 2 bytes */
					*iout++ = base64_get_1;
					*iout++ = base64_get_2;
					return true;
				}
			}
			else {
				if (b[3] != '=') return false;
				if (ibegin != iend) return false;
				/* 3n + 1 bytes */
				*iout++ = base64_get_1;
				return true;
			}
		}
	}

	if (i != 0) {
		return false;	
	}

	return true;

#undef base64_get_3
#undef base64_get_2
#undef base64_get_1
#undef base64_ref
}

#endif
