#ifndef AMC_SOCK_WRAPPER_H
#define AMC_SOCK_WRAPPER_H

#include "config.h"

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <unistd.h>
#include <arpa/inet.h>

#define ADDRSTRLEN INET6_ADDRSTRLEN

void getaddrinfo_w(const char *hostname, const char *service,
		const struct addrinfo *hints, struct addrinfo **result);



#endif
