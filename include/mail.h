#ifndef AMC_MAIL_H
#define AMC_MAIL_H

#include "config.h"
#include "utils.h"

#include <string>
#include <vector>
#include <utility>
#include <unordered_map>
#include <iostream>
#include <functional>

struct mail_t {
	struct mail_header_t {
		mail_header_t() {}

		template<class V>
		mail_header_t(
				V&& value, 
				int seq_no) :
			value(std::forward<V>(value)),
			seq_no(seq_no) {}

		mail_header_t(const mail_header_t&) = default;
		mail_header_t(mail_header_t&&) = default;
		mail_header_t& operator=(const mail_header_t&) = default;
		mail_header_t& operator=(mail_header_t&) = default;

		std::string value;
		int seq_no;
	};

	enum default_seq_no {
		MHEADER_DATE = 0,
		MHEADER_FROM,
		MHEADER_TO,
		MHEADER_CC,
		MHEADER_SUBJECT,
		
		MHEADER_X,
	};

	mail_t() = default;
	mail_t(const mail_t&) = default;
	mail_t(mail_t&&) = default;
	mail_t& operator=(const mail_t&) = default;
	mail_t& operator=(mail_t&&) = default;

	
	void set_date() {
		add_field("Date", get_local_time_str(), MHEADER_DATE);
	}
	void set_date(std::string date) {
		add_field("Date", std::move(date), MHEADER_DATE);
	}
	bool get_date(std::string &date) const {
		return get_field_value("Date", date);
	}

	void set_from(std::string mail_addr) {
		add_field("From", std::move(mail_addr), MHEADER_FROM);
	}
	bool get_from(std::string &mail_addr) const {
		return get_field_value("From", mail_addr);	
	}
	
	void add_to(std::string mail_addr) {
		append_comma_separated_values("To", mail_addr, MHEADER_TO);
	}
	bool get_to(std::vector<std::string>& mail_addrs) const {
		return get_comma_separated_values("To", mail_addrs);
	}

	void add_cc(std::string mail_addr) {
		append_comma_separated_values("cc", mail_addr, MHEADER_CC);
	}
	bool get_cc(std::vector<std::string>& mail_addrs) const {
		return get_comma_separated_values("cc", mail_addrs);
	}

	void set_subject(std::string subject) {
		add_field("Subject", std::move(subject), MHEADER_SUBJECT);
	}
	bool get_subject(std::string &subject) const {
		return get_field_value("Subject", subject);
	}
	
	template<class F, class V>
	void add_field(F&& field, V&& value, int seq_no) {
		insert_or_replace(headers, 
				std::forward<F>(field), 
				mail_header_t(std::forward<V>(value), seq_no));
	}

	void append_field(const std::string& field, const std::string& value) {
		mail_header_t *h;
		get_field(field, &h);
		if (!h) {
			add_field(field, value, MHEADER_X);
		}
		else {
			h->value.append("\r\n ").append(value);
		}
	}
	
	void get_field(const std::string& field, mail_header_t** h) {
		auto iter = headers.find(field);
		if (iter == headers.end()) 
			*h = nullptr;
		else 
			*h = &(iter->second);
	}

	void get_field(const std::string& field, const mail_header_t** h) const {
		const_cast<mail_t*>(this)->get_field(field, 
			const_cast<mail_header_t**>(h));
	}

	bool get_field_value(const std::string& field, std::string& value) const {
		const mail_header_t* h;
		get_field(field, &h);
		if (!h) return false;
		value = h->value;
		return true;
	}
	
	void append_comma_separated_values(const std::string& field, const std::string& value, int seq_no) {
		mail_header_t* h;
		get_field(field, &h);
		if (!h) {
			add_field(field, value, seq_no);
		}
		else {
			h->value.append(",\r\n ").append(value);	
		}
	}

	bool get_comma_separated_values(
			const std::string& field, 
			std::vector<std::string> &values) const;
	
	void text_append_line(const std::string& line) {
		if (line.length() >= 1 && line[0] == '.') {
			text.append("..").append(line.begin() + 1, line.end()).append("\r\n");
		}
		else text.append(line).append("\r\n");
	}

	std::string text;
	std::unordered_map<std::string, mail_header_t> headers;
	
	std::vector<decltype(std::cref(*headers.begin()))> get_sorted_headers() const;

	void get_mail_body(std::string &body) const {
		get_mail_body(body, "\r\n");
	}

	void get_mail_body(std::string &body, const char *eoln) const;
};

void dump_mail(std::ostream& os, const mail_t& mail);

#endif
