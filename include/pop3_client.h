#ifndef AMC_POP3_CLIENT_H
#define AMC_POP3_CLIENT_H

#include "config.h"
#include "client_socket.h"

#include <string>
#include <vector>
#include <iostream>

struct mail_info_t {
	int no;
	int len;
};

class pop3_client_t {
public:
	pop3_client_t(std::string server,
			std::string port,
			std::string user,
			std::string pass) :
		server(server), port(port),
		user(user), pass(pass) {}

	void recv_all(bool del = false);
	void recv_all(std::string filename, bool del = false);

	void recv_all(std::ostream& os, bool del = false);

private:
	void recv_greeting(connection_t& con);

	void do_auth(connection_t& con);

	int stat(connection_t& con);
	
	void list(connection_t& con, 
			std::vector<mail_info_t>& v);

	void list(connection_t& con, int msg_no, 
			std::vector<mail_info_t>& v);

	std::string retr(connection_t& con, int msg_no);	

	void dele(connection_t& con, int msg_no);
	
	void quit_early(connection_t& con) noexcept;
	void quit(connection_t& con);

	bool is_ok(const std::string& msg) noexcept {
		return msg.length() >= 3 && msg[0] == '+'
			 && msg.substr(1, 2) == "OK";
	}

	bool is_err(const std::string& msg) noexcept {
		return msg.length() >= 4 && msg[0] == '-'
			 && msg.substr(1, 3) == "ERR";
	}

	std::string server;
	std::string port;
	std::string user;
	std::string pass;
};

#endif
