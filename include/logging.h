#ifndef AMC_LOGGING_H
#define AMC_LOGGING_H

#include "config.h"
#include <string>
#include <memory>
#include <utility>

class event_logger;

enum component_t {
	COMP_UNKNOWN = 0,
	COMP_CONNECTION,
	COMP_OTHER,
	COMP_SYS,
	COMP_POP3,
	COMP_SMTP,
	COMP_MAIN,

	N_COMPONENT
};

enum logging_level_t {
	LLEVEL_FATAL = 0,
	LLEVEL_ERROR,
	LLEVEL_WARN,
	LLEVEL_INFO,
	LLEVEL_DEBUG,

	N_LOGGING_LEVEL
};

constexpr logging_level_t LLEVEL_NO_LOG = N_LOGGING_LEVEL;
constexpr logging_level_t LLEVEL_ERROR_ONLY = LLEVEL_ERROR;

void init_default_event_loggers(
		logging_level_t level, 
		const char* log_filename);

const std::string& strcomponent(component_t comp_id);

inline const char* cstrcomponent(component_t comp_id) {
	return strcomponent(comp_id).c_str();
}

const std::string& strlogginglevel(logging_level_t level);

inline const char* cstrlogginglevel(logging_level_t level) {
	return strlogginglevel(level).c_str();
}

std::string debug_format_event(
	logging_level_t event_level,
	component_t comp_id,
	int _errno,
	std::string msg,
	const char *file,
	const char *lineno);

std::string info_format_event(
	logging_level_t event_level,
	component_t comp_id,
	int _errno,
	std::string msg,
	const char *file,
	const char *lineno);

void log_event(
	logging_level_t event_level,
	component_t comp_id,
	int _errno,
	std::string msg,
	const char *file,
	const char *lineno);

inline void log_event(
	logging_level_t event_level,
	component_t comp_id,
	std::string msg,
	const char *file,
	const char *lineno) {
	
	log_event(event_level,
			comp_id,
			0,
			msg,
			file,
			lineno);
}

void add_logger(std::unique_ptr<event_logger> p_logger);

void add_file_logger(
		logging_level_t logging_level, 
		const char *filename, const char *mode);

void add_file_logger(
		logging_level_t logging_level,
		FILE *fp);

void clear_loggers();

class event_logger {
public:
	event_logger(logging_level_t level):
		level(level) {}

	virtual ~event_logger() {}

	void logging_level(logging_level_t level) {
		this->level = level;
	}

	logging_level_t logging_level() const {
		return this->level;
	}

	virtual void log_event(
			logging_level_t event_level,
			component_t comp_id,
			int _errno,
			std::string msg,
			const char *file,
			const char *lineno) = 0;

protected:
	logging_level_t level;

	std::string get_default_logging_string(
			logging_level_t event_level,
			component_t comp_id,
			int _errno,
			std::string msg,
			const char *file,
			const char *lineno) {
		return  (logging_level() == LLEVEL_DEBUG) ?
			debug_format_event(
				event_level, comp_id, _errno, std::move(msg), file, lineno)
		:	info_format_event(
				event_level, comp_id, _errno, std::move(msg), file, lineno);
	}

};

#endif

