#ifndef AMC_SMTP_CLIENT_H
#define AMC_SMTP_CLIENT_H

#include "config.h"

#include "client_socket.h"
#include "mail.h"
#include <string>
#include <cstdlib>

class smtp_client_t {
public:
	smtp_client_t(std::string server,
			std::string port,
			std::string email,
			std::string pass);

	void send_mail(mail_t& mail);

	std::string get_email_addr() const {
		return user + "@" + domain;
	}
	
private:
	void check_mail(mail_t& mail);
		
	void recv_greeting(connection_t& con);

	void ehlo(connection_t& con);

	void auth_login(connection_t& con);
	void auth_plain(connection_t& con);
	
	void mail_from(connection_t& con);
	void mail_to(connection_t& con);

	void mail_data(connection_t& con, mail_t &mail);

	void quit_early(connection_t& con) noexcept;
	void quit(connection_t& con);

	std::string server, port, user, domain, pass;
	std::string _mail_from;
	std::vector<std::string> _mail_to;

	static bool start_with(const std::string& msg, int status_code) {
		if (msg.length() >= 3) {
			std::string s = msg.substr(0, 3);
			return status_code == std::atoi(s.c_str());
		}
		else return false;
	}

	static bool has_next_line(const std::string& msg) {
		return msg.length() >= 4 && msg[3] == '-';
	}

	static bool check_recipient_format(const std::string& s, std::string& addr_part);

	bool support_login;
	bool support_plain;
};

#endif
