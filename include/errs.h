#ifndef AMC_ERRS_H
#define AMC_ERRS_H

#include "config.h"

#include <exception>
#include <string>

#include "macros.h"
#include "utils.h"
#include "logging.h"

class exception_base;

#define throw_exception(level, comp_id, errno, msg)	\
	do {	\
		log_event(level, comp_id, errno, msg, __FILE__, STRINGIFY(__LINE__));	\
		throw exception_base(errno);	\
	} while(0)

#define throw_fatal_eno(comp_id, errno, msg)	\
	throw_exception(LLEVEL_FATAL, comp_id, errno, msg)

#define throw_error_eno(comp_id, errno, msg)	\
	throw_exception(LLEVEL_ERROR, comp_id, errno, msg)

#define throw_fatal(comp_id, msg)	\
	throw_fatal_eno(comp_id, 0, msg)

#define throw_error(comp_id, msg)	\
	throw_error_eno(comp_id, 0, msg)

#define log_warn(comp_id, msg)	\
	log_event(LLEVEL_WARN, comp_id, 0, msg, __FILE__, STRINGIFY(__LINE__))

#define log_info(comp_id, msg)	\
	log_event(LLEVEL_INFO, comp_id, 0, msg, __FILE__, STRINGIFY(__LINE__))

#define log_debug(comp_id, msg)	\
	log_event(LLEVEL_DEBUG, comp_id, 0, msg, __FILE__, STRINGIFY(__LINE__))

#define throw_fatal_eno_f(comp_id, errno, ...)	\
	throw_fatal_eno(comp_id, errno, format_string("format error", __VA_ARGS__))

#define throw_error_eno_f(comp_id, errno, ...)	\
	throw_error_eno(comp_id, errno, format_string("format error", __VA_ARGS__))

#define throw_fatal_f(comp_id, ...)	\
	throw_fatal(comp_id, format_string("format error", __VA_ARGS__))

#define throw_error_f(comp_id, ...)	\
	throw_error(comp_id, format_string("format error", __VA_ARGS__))

#define log_warn_f(comp_id, ...)	\
	log_warn(comp_id, format_string("format error", __VA_ARGS__))

#define log_info_f(comp_id, ...)	\
	log_info(comp_id, format_string("format error", __VA_ARGS__))

#define log_debug_f(comp_id, ...)	\
	log_debug(comp_id, format_string("format error", __VA_ARGS__))

#ifndef NDEBUG
#	define log_verbose(comp_id, msg) log_debug(comp_id, msg)
#	define log_verbose_f(comp_id, ...) \
	log_debug_f(comp_id, __VA_ARGS__)
#else
#	define log_verbose(...)
#	define log_verbose_f(...)
#endif

/* implementation details */


class exception_base: public std::exception {
public:
	exception_base(int e): _errno(e),
		s_errno(to_string(_errno)) {}

	exception_base(
			const exception_base& e) = default;

	exception_base& operator=(const exception_base& e) = default;
	
	const int _errno;
	const std::string s_errno;

	virtual const char* what() const noexcept {
		return s_errno.c_str();
	}
}; 

#endif
