#ifndef AMC_MACROS_H
#define AMC_MACROS_H

#include "config.h"

#define CONCATHELPER(s1,s2) s1##s2
#define CONCAT(s1, s2) CONCATHELPER(s1,s2)

#define STRINGIFYHELPER(m) #m
#define STRINGIFY(num) STRINGIFYHELPER(num)


#endif
