#ifndef AMC_SOCKET_CONNECTION_H
#define AMC_SOCKET_CONNECTION_H

#include "config.h"
#include "connection.h"
#include <string>

class client_socket_t: public connection_t {
public:

	client_socket_t(
			EOLN_style eoln_style,
			size_t buf_size = DEFAULT_BUFFER_SIZE)
		: connection_t(eoln_style, buf_size),
		sockfd(-1) {}

	virtual ~client_socket_t();

	void init_conn(std::string host, std::string port);

	void init_conn(std::string host, unsigned short port) {
		init_conn(host, to_string(port));
	}
	
	void close();

	virtual bool get_local_name(std::string *name, short *port);

protected:
	int sockfd;	

	virtual ssize_t read_no_buf(char *buffer, size_t buf_size) noexcept;
	virtual ssize_t write_no_buf(const char *buffer, size_t len) noexcept;

};

#endif

