#ifndef AMC_CONNECTION_H
#define AMC_CONNECTION_H

#include "config.h"

#include <string>
#include <utility>

class connection_t {
public:
	enum EOLN_style {
		EOLN_CRLF  = 0, EOLN_CR, EOLN_LF,

		NEOLN_STYLE
	};

	connection_t(EOLN_style eoln_style, 
			size_t buf_size = DEFAULT_BUFFER_SIZE);
	virtual ~connection_t();

	connection_t& operator=(const connection_t&) = delete;
	connection_t& operator=(connection_t&&) = delete;

	ssize_t read(char *buffer, size_t buf_size) noexcept;

	ssize_t write(const char *buffer, size_t len) noexcept;
	
	std::string readline() {
		std::string buf;
		readline(buf);
		return std::move(buf);
	}

	/**
	 * read a line and append it to output
	 * line terminator is discarded.
	 * */
	void readline(std::string &output);

	void writeline(std::string msg);

	const char* get_eoln() const noexcept {
		return eoln_cstr[eoln_style];
	}

	ssize_t get_eoln_len() const noexcept {
		return eoln_skip_cnt[eoln_style];
	}

	void resize_buffer(size_t buf_size) noexcept;

	void reset_buffer() noexcept {
		resize_buffer(buf_size);
	}

	virtual bool get_local_name(std::string *name, short *port) {
		return false;
	}

protected:

	virtual ssize_t read_no_buf(char *buffer, size_t buf_size) noexcept = 0;
	virtual ssize_t write_no_buf(const char *buffer, size_t len) noexcept = 0;

private:
	EOLN_style eoln_style;
	
	size_t buf_size;
	ssize_t buf_head;
	ssize_t buf_n;
	char *buffer;
	
	ssize_t find_eoln(char *buffer, ssize_t buf_n) noexcept;

	static const char *eoln_cstr[NEOLN_STYLE];
	static const ssize_t eoln_skip_cnt[NEOLN_STYLE];
};

#endif
