#ifndef AMC_UTILS_H
#define AMC_UTILS_H

#include "config.h"
#include <string>
#include <utility>
#include <cstring>

std::string format_string(const char *format, ...)
amc_attribute((format(printf, 1, 2)));

std::string format_string(const char *default_string,
		const char *format, ...) noexcept
amc_attribute((format(printf, 2, 3))) ;

std::string get_input(std::string prompt);

std::string get_pass(std::string prompt);

std::string get_local_time_str();

template<class MapContainer, class Key, class ...Args>
void emplace_or_replace(MapContainer &m, Key&& k, Args&&... args) {
	auto iter = m.find(k);
	if (iter != m.end()) {
		iter->second = typename MapContainer::mapped_type(args...);
	}
	else {
		m.emplace(std::forward<Key>(k), typename MapContainer::mapped_type(args...));
	}
}

template<class MapContainer, class Key, class Value>
void insert_or_replace(MapContainer &m, Key&& k, Value&& value) {
	auto iter = m.find(k);
	if (iter != m.end()) {
		iter->second = std::forward<Value>(value);
	}
	else {
		m.emplace(std::forward<Key>(k), std::forward<Value>(value));
	}
}

inline bool ci_eq_c(char x, char y) {
	return tolower(x) == tolower(y);
}

template<class It1, class It2>
bool ci_eq_str(It1 b1, It1 e1, It2 b2, It2 e2) {
	while (b1 != e1 && b2 != e2) {
		if (tolower(*b1++) != tolower(*b2++))
			return false;
	}

	return b1 == e1 && b2 == e2;
}

template<class It1>
bool ci_eq_str(It1 b1, It1 e1, const char *s2) {
	while (b1 != e1 && *s2) {
		if (tolower(*b1++) != tolower(*s2++)) return false;	
	}
	return b1 == e1 && !*s2;
}

template<class It2>
bool ci_eq_str(const char *s1, It2 b1, It2 b2) {
	return ci_eq_str(b1, b2, s1);
}

inline bool ci_eq_str(const char *b1, const char *e1, const char *s2) {
	while (b1 != e1 && *s2) {
		if (tolower(*b1++) != tolower(*s2++)) return false;	
	}
	return b1 == e1 && !*s2;
}

inline bool ci_eq_str(const char* s1, const char* s2) {
	while (*s1 && *s2) {
		if (tolower(*s1++) != tolower(*s2++)) return false;
	}
	return !*s1 && !*s2;
}

inline bool is_sp(char c) {
	return c == ' ' || c == '\t';
}

inline void skip_sp(const std::string& v, decltype(v.size())& i) {
	while (i < v.length() && is_sp(v[i])) ++i;
}

extern "C" {
	extern int rfc822_special_table[256];
}

inline bool is_special(char c) {
	return rfc822_special_table[(unsigned) c];
}

typename std::string::size_type
get_next_name_email(const std::string &v,
		typename std::string::size_type i, 
		std::string &name, 
		std::string &mail_addr);

inline std::string get_formated_addr(const std::string &name,
		const std::string &addr) {
	std::string f;
	f.reserve(5 + name.length() + addr.length());
	if (!name.empty())
		f.append("\"").append(name).append("\" ");
	f.append("<").append(addr).append(">");
	return f;
}
#endif
