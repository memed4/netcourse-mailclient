CC=gcc -std=gnu11
CXX=g++ -std=gnu++11
MKDIR=mkdir -p
OPENSSL=/usr/local/ssl

SRCDIR=src
TESTDIR=test
DEPSDIR=deps
BINDIR=bin
OBJSDIR=objs
TMPDIR=tmp

INCLUDES=-I./ -I./include/
DEFS=-DNDEBUG
M_CXXFLAGS=$(DEFS) -g -Wall
M_CFLAGS=$(DEFS) -g -Wall

CXXFLAGS+= $(M_CXXFLAGS) $(INCLUDES)
CFLAGS+= $(M_CFLAGS) $(INCLUDES)

MAINSRCS= \
	pop3_main.cpp \
	smtp_main.cpp

CXX_SRCS=\
	connection.cpp \
	logging.cpp	\
	utils.cpp \
	sock_wrapper.cpp \
	client_socket.cpp \
	pop3_client.cpp	\
	mail.cpp \
	smtp_client.cpp

C_SRCS=\
	encoding.c	\
	utils_c.c

MAINS=$(MAINSRCS:.cpp=)
MAIN_OBJS=$(MAINSRCS:.cpp=.o)

CXX_OBJS=$(CXX_SRCS:.cpp=.o)
C_OBJS=$(C_SRCS:.c=.o)

OBJS=$(CXX_OBJS) $(C_OBJS)
POBJS=$(addprefix $(OBJSDIR)/,$(OBJS))
LIBS=

TESTS=\
	test_debug_level_logger \
	test_error_level_logger \
	test_readline \
	test_base64

.PHONY: all test clean cleanall

all: $(addprefix $(OBJSDIR)/,$(OBJS)) $(addprefix $(BINDIR)/,$(MAINS))

$(DEPSDIR):
	@$(MKDIR) $(DEPSDIR)

$(BINDIR):
	@$(MKDIR) $(BINDIR)

$(OBJSDIR):
	@$(MKDIR) $(OBJSDIR)

$(addprefix $(OBJSDIR)/,$(CXX_OBJS) $(MAIN_OBJS)): $(OBJSDIR)/%.o: $(SRCDIR)/%.cpp | $(OBJSDIR) $(DEPSDIR)
	$(CXX) $(CXXFLAGS) -c $< -o $@
	$(CXX) $(INCLUDES) -MM $< > $(DEPSDIR)/$*.d.tmp
	@sed -e 's,.*:,$@:,' < $(DEPSDIR)/$*.d.tmp > $(DEPSDIR)/$*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $(DEPSDIR)/$*.d.tmp \
		| fmt -1 \
		| sed -e 's/^ *//' -e 's/$$/:/' >> $(DEPSDIR)/$*.d
	@rm -f $(DEPSDIR)/$*.d.tmp

$(addprefix $(OBJSDIR)/,$(C_OBJS)): $(OBJSDIR)/%.o: $(SRCDIR)/%.c | $(OBJSDIR) $(DEPSDIR)
	$(CC) $(CFLAGS) -c $< -o $@
	$(CC) $(INCLUDES) -MM $< > $(DEPSDIR)/$*.d.tmp
	@sed -e 's,.*:,$@:,' < $(DEPSDIR)/$*.d.tmp > $(DEPSDIR)/$*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $(DEPSDIR)/$*.d.tmp \
		| fmt -1 \
		| sed -e 's/^ *//' -e 's/$$/:/' >> $(DEPSDIR)/$*.d
	@rm -f $(DEPSDIR)/$*.d.tmp

-include $(addprefix $(DEPSDIR)/,$(CXX_SRCS:.cpp=.d))

-include $(addprefix $(DEPSDIR)/,$(C_SRCS:.c=.d))

test: $(addprefix $(BINDIR)/,$(TESTS))
	@./run_tests.sh $(BINDIR) $(TMPDIR) "test.log"

bin/test_debug_level_logger: test/test_logging.cpp $(POBJS) | $(BINDIR)
	$(CXX) -DLOGGING_LEVEL=LLEVEL_DEBUG $(CXXFLAGS) $^ -o $@ $(LIBS)

bin/test_error_level_logger: test/test_logging.cpp $(POBJS) | $(BINDIR)
	$(CXX) -DLOGGING_LEVEL=LLEVEL_ERROR $(CXXFLAGS) $^ -o $@ $(LIBS)

bin/test_readline: test/test_readline.cpp $(POBJS) | $(BINDIR)
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LIBS)

bin/test_base64: test/test_base64.cpp $(POBJS) | $(BINDIR)
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LIBS)

bin/pop3_main: $(POBJS) $(OBJSDIR)/pop3_main.o | $(BINDIR)
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LIBS)

bin/smtp_main: $(POBJS) $(OBJSDIR)/smtp_main.o | $(BINDIR)
	$(CXX) $(CXXFLAGS) $^ -o $@ $(LIBS)

clean:
	rm -rf $(BINDIR) $(OBJSDIR) $(TMPDIR) "test.log"

cleanall: clean
	rm -rf $(DEPSDIR)


